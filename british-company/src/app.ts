interface ILocation {
  addPerson(person: Employee): void;
  getPerson(index: number): Employee;
  getCount(): number;
}

class CompanyLocationArray implements ILocation {
  private employees: Employee[] = [];

  public addPerson(person: Employee): void {
    this.employees.push(person);
  }

  public getPerson(index: number): Employee {
    return this.employees[index];
  }

  public getCount(): number {
    return this.employees.length;
  }
}

class CompanyLocationLocalStorage implements ILocation {
  private static STORAGE_KEY = "employees";

  constructor() {
    const data = localStorage.getItem(CompanyLocationLocalStorage.STORAGE_KEY);
    if (!data) {
      localStorage.setItem(
        CompanyLocationLocalStorage.STORAGE_KEY,
        JSON.stringify([])
      );
    }
  }

  public addPerson(person: Employee): void {
    const employees = this.getEmployeesFromStorage();
    employees.push(person);
    localStorage.setItem(
      CompanyLocationLocalStorage.STORAGE_KEY,
      JSON.stringify(employees)
    );
  }

  public getPerson(index: number): Employee {
    const employees = this.getEmployeesFromStorage();
    return employees[index];
  }

  public getCount(): number {
    const employees = this.getEmployeesFromStorage();
    return employees.length;
  }

  private getEmployeesFromStorage(): Employee[] {
    const data = localStorage.getItem(CompanyLocationLocalStorage.STORAGE_KEY);
    const employees = JSON.parse(data) as any[];
    return employees.map((emp) => new Employee(emp.name, emp.currentProject));
  }
}

class Employee {
  private name: string;
  private currentProject: string;

  constructor(name: string, currentProject: string) {
    this.name = name;
    this.currentProject = currentProject;
  }

  public getCurrentProject(): string {
    return this.currentProject;
  }

  public getName(): string {
    return this.name;
  }
}

class Company<Location extends ILocation> {
  private location: Location;

  constructor(location: Location) {
    this.location = location;
  }

  public add(employee: Employee): void {
    this.location.addPerson(employee);
  }

  public getProjectList(): string[] {
    const projects: string[] = [];
    const length = this.location.getCount();
    for (let index = 0; index < length; index++) {
      const person = this.location.getPerson(index);
      projects.push(person.getCurrentProject());
    }
    return projects;
  }

  public getNameList(): string[] {
    const names: string[] = [];
    const length = this.location.getCount();
    for (let index = 0; index < length; index++) {
      const person = this.location.getPerson(index);
      names.push(person.getName());
    }
    return names;
  }
}

// Creating companies
const companyLocationArray = new Company(new CompanyLocationArray());
const companyLocationLocalStorage = new Company(
  new CompanyLocationLocalStorage()
);

// Creating objects of employees
const employee1 = new Employee("Alice", "Project A");
const employee2 = new Employee("Bob", "Project B");
const employee3 = new Employee("John", "Project C");
const employee4 = new Employee("Janice", "Project D");
const employee5 = new Employee("Mark", "Project E");
const employee6 = new Employee("Ana", "Project F");

// Adding employees to the companies
companyLocationArray.add(employee1);
companyLocationArray.add(employee2);
companyLocationArray.add(employee3);
companyLocationLocalStorage.add(employee4);
companyLocationLocalStorage.add(employee5);
companyLocationLocalStorage.add(employee6);

// Displaying project list and name list in each company
console.log("Company location array");
console.log("Project List:", companyLocationArray.getProjectList().join(", "));
console.log("Name List:", companyLocationArray.getNameList().join(", "));

console.log("Company location local storage");
console.log(
  "Project List:",
  companyLocationLocalStorage.getProjectList().join(", ")
);
console.log("Name List:", companyLocationLocalStorage.getNameList().join(", "));

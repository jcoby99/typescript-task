interface IEmployee {
  getCurrentProject(): string;
  getName(): string;
}

class Company {
  private employees: IEmployee[] = [];

  public add(employee: IEmployee): void {
    this.employees.push(employee);
  }

  public getProjectList(): string[] {
    return this.employees.map((employee) => employee.getCurrentProject());
  }

  public getNameList(): string[] {
    return this.employees.map((employee) => employee.getName());
  }
}

class Backend implements IEmployee {
  constructor(private name: string, private currentProject: string) {}

  public getCurrentProject(): string {
    return this.currentProject;
  }
  public getName(): string {
    return this.name;
  }
}

class Frontend implements IEmployee {
  constructor(private name: string, private currentProject: string) {}

  public getCurrentProject(): string {
    return this.currentProject;
  }
  public getName(): string {
    return this.name;
  }
}

// Creating object of company class
const company = new Company();

// Creating objects of Frontend employees
const frontendEmployee1 = new Frontend("Alice", "Project A");
const frontendEmployee2 = new Frontend("Bob", "Project B");
const frontendEmployee3 = new Frontend("John", "Project C");

// Creating objects of Backend employees
const backendEmployee1 = new Backend("Janice", "Project D");
const backendEmployee2 = new Backend("Mark", "Project E");
const backendEmployee3 = new Backend("Ana", "Project F");

// Adding employees to the company
company.add(frontendEmployee1);
company.add(frontendEmployee2);
company.add(frontendEmployee3);
company.add(backendEmployee1);
company.add(backendEmployee2);
company.add(backendEmployee3);

// Displaying project list and name list
console.log("Project List:", company.getProjectList().join(", "));
console.log("Name List:", company.getNameList().join(", "));
